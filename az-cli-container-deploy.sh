#!/bin/bash



# Basic while loop
counter=1
while [ $counter -le 10 ]
do
    az deployment group create \
        --parameters '{ "location": {"value":"eastus2"}}' \
        --resource-group xmruseast2 \
        --name deploy-xmr-container \
        --template-uri  "https://bitbucket.org/bardlehel/xmr-container-az-deploy/raw/f8eb0b00ad0752fb2808e401e39edb3e2765573e/xmr-container-template.json" 
    ((counter++))
done
